# 4images - Image Gallery Management System (MOD)

## Licence

Please see the `docs/Licence.english.txt` file

If you want use 4images for non-private and commercial  purposes or if you want to remove the copyright text in the footer of the gallery, please get your license here: http://www.4homepages.de/download-4images/

## Installation

Please see the `docs/Installation.english.txt` file

## FAQ

If you have problems, see the FAQ section in our support forum: http://www.4homepages.de/forum/index.php?board=14.0

## Download

You can get the newest version of 4images at http://www.4homepages.de

## Support

There is a support forum under http://www.4homepages.de/forum


## Lizenz / Online-Lizenzshop

Die Lizenzbestimmungen finden Sie in der Datei `docs/Lizenz.deutsch.txt`

Wenn Sie 4images für nicht private Zwecke nutzen möchten oder Sie den Copyright-Vermerk im Fuß der Galerie entfernen möchten, erwerben Sie bitte hier die entsprechende Lizenz: http://www.4homepages.de/download-4images/

## Installation

Eine Installationsanleitung finden Sie in der Datei `docs/Installation.deutsch.txt`

## FAQ

Bei Problemen kontaktieren Sie bitte den FAQ-Bereich in unserem Support-Forum: http://www.4homepages.de/forum/index.php?board=14.0

## Download

Die neueste Version von 4images kann unter http://www.4homepages.de heruntergeladen werden.

## Support

Das Supportforum finden Sie unter http://www.4homepages.de/forum

# DEUTSCH / GERMAN
4images kann komplett browserbasiert über einen passwortgeschützten Administrationsbereich verwaltet werden. Dabei ist man nicht nur auf die gängigen Bildformate wie JPG, GIF und PNG beschränkt, 4images lässt sich leicht für jedes beliebige Dateiformat erweitern.

Aufgrund der Trennung von Design und Programmcode läßt sich 4images über sein Templatesystem komplett an das Design der eigenen Website anpassen. Eine externe Sprachdatei erlaubt darüber hinaus eine einfache Übersetzung in andere Sprachen.

Weitere Features von 4images sind unter anderem: komplette Userverwaltung mit Registrierung, Kommentarsystem zu jedem Bild, Leuchtkastenfunktion für registrierte Benutzer, umfangreiche Suchfunktion, RSS-Feeds und vieles mehr.

### Hier sehen Sie ein Übersicht der Features die 4images bietet:

- Allgemein
  - Umfangreicher, passwortgeschützter Administrationsbereich über den das gesamte System gepflegt werden kann
  - integrierte Backup-Funktion für die Datenbank
  - mehrsprachig über "Language-Packs" möglich
  - Installer für eine einfache Installation der Software
  - RSS Feeds (neue Bilder der Galerie, neue Bilder einer Kategorie, neue Kommentare eines Bildes)

- Kategorien
  - Es können beliebig viele Kategorien und Unterkategorien angelegt und verschachtelt werden
  - Erfassung der Klicks auf eine Kategorie
  - Optionaler Beschreibungstext für jede Kategorie
  - Anzeige der Unterkategorien unter der Hauptkategorie. Anzahl der angezeigten Unterkategorien einstellbar

- Bilder
  - Standardmäßig integrierte Bild- und Dateiformate:
  - jpg, gif, png, aif, au, avi, mid, mov, mp3, mpg, swf, wav, ra, rm, zip, pdf
  - Erweiterung um jedes beliebige Dateiformat (3gp, psd, smil, etc.)
  - Upload der Dateien browserbasiert über den Administrationsbereich oder per FTP
  - Automatische Thumbnail-Erstellung über den Administrationsbereich (Voraussetzung: ImageMagick, GD oder NetPBM)
  - Automatische Bildgrößen-Konvertierung über den Administrationsbereich (Voraussetzung: ImageMagick, GD oder NetPBM)
  - Auslesen und Anzeigen von IPTC- und EXIF-Daten
  - Bewertungfunktion durch Besucher für jedes Bild
  - Individuell aktivierbare Kommentarfunktion für jedes Bild (weitere Informationen siehe unten)
  - Downloadfunktion für jedes Bild mit Zählfunktion
  - Download ausgewählter Bilder als zip-Archiv
  - Angabe von Beschreibungstext, Keywords für jedes Bild
  - konfigurierbare E-Card Funktion (Farben, Schrift)
  - Schutz vor "hotlinking"

- Kommentare
  - Individuell aktivierbare Kommentarfunktion für jedes Bild
  - Unterstützung von BB-Code (Einbinden von Bilder per BB-Code in den Kommentaren optional abstellbar)
  - HTML-Code in den Kommentaren kann unterbunden werden
  - Spam Kontrolle
  - Badword-Funktion
  - Automatische Umbrechen von langen Wörtern (Zeichenanzahl einstellbar)

- User
  - Komplette Userverwaltung mit Registrierungsfunktion (double opt-in)
  - Umfangreiche Befugnis-Verwaltung für einzelne User und Usergruppen
  - Leuchtkastenfunktion für registrierte Benutzer
  - Upload-Funktion für User
  - Modul zur Anzeige der User die gerade online sind
  - Newsletterfunktion
  - Verschlüsselte Speicherung der Passwörter in der Datenbank

- Templates
  - Design und Layout können über Templates an das Design der eigenen Website angepasst werden
  - Bearbeitung mit jedem beliebigen HTML-Editor oder über den Administrationsbereich
  - Standard Template-Pack inklusive
  - weitere Templates als Download verfügbar

### Systemvorraussetzungen zum Betrieb von 4images:

- Ein Webserver oder ein Webhosting Account (Empfohlen wird Linux/Apache)
- PHP Version 4.0.5 oder höher (Empfohlen wird jeweils die aktuelle stabile Version zu verwenden)).
- Damit 4images korrekt funktioniert, sollte safe_mode abgeschaltet sein
- (safe_mode = off in der php.ini).
- MySQL Version 3.23 oder höher (Empfohlen wird MySQL Version 3.23.33)

- Optional:
  - [ImageMagick](https://imagemagick.org/index.php) oder [GD](https://libgd.github.io/)