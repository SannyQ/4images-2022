<?php
/**************************************************************************
 *                                                                        *
 *    4images - A Web Based Image Gallery Management System               *
 *    ----------------------------------------------------------------    *
 *                                                                        *
 *             File: db_mysqli.php                                         *
  *        Copyright: (C) 2002-2016 4homepages.de                          *
 *            Email: 4images@4homepages.de                                * 
 *              Web: http://www.4homepages.de                             * 
 *    Scriptversion: 1.8                                                  *
 *                                                                        *
 **************************************************************************
 *                                                                        *
 *    Dieses Script ist KEINE Freeware. Bitte lesen Sie die Lizenz-       *
 *    bedingungen (Lizenz.txt) fÃ¼r weitere Informationen.                 *
 *    ---------------------------------------------------------------     *
 *    This script is NOT freeware! Please read the Copyright Notice       *
 *    (Licence.txt) for further information.                              *
 *                                                                        *
 *************************************************************************/
if (!defined('ROOT_PATH')) {
  die("Security violation");
}

class Db
{
	var $no_error = 0;
	var $connection;
	var $query_id = 0;
	var $query_count = 0;
	var $query_time = 0;
	var $query_array = array();
	var $table_fields = array();

	function __construct($db_host, $db_user, $db_password = "", $db_name = "", $db_pconnect = 0)
	{
		$connect_handle = "mysqli_connect";
		if (!$this->connection = @$connect_handle($db_host, $db_user, $db_password)) {
			$this->error("Could not connect to the database server (".safe_htmlspecialchars($db_host).", ".safe_htmlspecialchars($db_user).").", 1);
		}
		if ($db_name != "") {
			if (!mysqli_select_db($this->connection, $db_name)) {
				@mysqli_close($this->connection);
				$this->error("Could not select database (".safe_htmlspecialchars($db_name).").", 1);
			}
		}
		// 07.10.2017  KPL   Auskommentiert, da dadurch die Umlaute auf der Website falsch dargestellt werden
		// mysqli_set_charset($this->connection, 'utf8');
		return $this->connection;
	}

	function escape($value)
	{
		return mysqli_real_escape_string($this->connection, $value);
	}

	function close()
	{
		if ($this->connection) {
			// 22.8.2021 Lexow : Verhindert die Fehler "Error: mysqli_result object is already closed in"	und
			// "Fatal error: Uncaught Error: mysqli_result object is already closed"
			// Wenn query (quasi) leer ist, dann wird entweder ein Boolean=true oder eine leeres Objekt (ohne Items geliefert)
			//  if ($this->query_id)  {
			if (($this->query_id) and (isset($this->query_id->field_count))) {
				@mysqli_free_result($this->query_id);
			}
			return @mysqli_close($this->connection);
		} else {
			return false;
		}
	}

	function query($query = "")
	{
		unset($this->query_id);
		if ($query != "") {
			if ((defined("PRINT_QUERIES") && PRINT_QUERIES == 1) || (defined("PRINT_STATS") && PRINT_STATS == 1)) {
				$startsqltime = explode(" ", microtime());
			}
			if (!$this->query_id = @mysqli_query($this->connection, $query)) {
				$this->error("<b>Bad SQL Query</b>: ".safe_htmlspecialchars($query)."<br /><b>".safe_htmlspecialchars(mysqli_error($this->connection))."</b>");
			}
			if ((defined("PRINT_QUERIES") && PRINT_QUERIES == 1) || (defined("PRINT_STATS") && PRINT_STATS == 1)) {
				$endsqltime = explode(" ", microtime());
				$totalsqltime = round($endsqltime[0]-$startsqltime[0]+$endsqltime[1]-$startsqltime[1],3);
				$this->query_time += $totalsqltime;
				$this->query_count++;
			}
			if (defined("PRINT_QUERIES") && PRINT_QUERIES == 1) {
				$query_stats = htmlentities($query);
				$query_stats .= "<br><b>Querytime:</b> ".$totalsqltime;
				$this->query_array[] = $query_stats;
			}
			return $this->query_id;
		}
	}

	function fetch_array($query_id = -1, $assoc = 0)
	{
		if ($query_id != -1) {
			$this->query_id = $query_id;
		}
		if ($this->query_id) {
			return ($assoc) ? mysqli_fetch_assoc($this->query_id) : mysqli_fetch_array($this->query_id);
		}
	}

	function free_result($query_id = -1)
	{
		if ($query_id != -1) {
			$this->query_id = $query_id;
		}
		// 22.8.2021 Lexow : Verhindert die Fehler "Error: mysqli_result object is already closed in"	und
		// "Fatal error: Uncaught Error: mysqli_result object is already closed"
		// Wenn query (quasi) leer ist, dann wird entweder ein Boolean=true oder eine leeres Objekt (ohne Items geliefert)
		if (isset($this->query_id->field_count)) {
			return @mysqli_free_result($this->query_id);
		} else
			return false;
	}

	function query_firstrow($query = "")
	{
		if ($query != "") {
			$this->query($query);
		}
		$result = $this->fetch_array($this->query_id);
		$this->free_result();
		return $result;
	}

	function get_numrows($query_id = -1)
	{
		if ($query_id != -1) {
			$this->query_id = $query_id;
		}
		return mysqli_num_rows($this->query_id);
	}

	function get_insert_id()
	{
		return ($this->connection) ? @mysqli_insert_id($this->connection) : 0;
	}

	function get_next_id($column = "", $table = "")
	{
		if (!empty($column) && !empty($table)) {
			$sql = "SELECT MAX($column) AS max_id
              FROM $table";
			$row = $this->query_firstrow($sql);
			return (($row['max_id'] + 1) > 0) ? $row['max_id'] + 1 : 1;
		} else {
			return NULL;
		}
	}

	function get_numfields($query_id = -1)
	{
		if ($query_id != -1) {
			$this->query_id = $query_id;
		}
		return @mysqli_num_fields($this->query_id);
	}

	//  function get_fieldname($query_id = -1, $offset) {
	function get_fieldname($offset, $query_id = -1) {  // 22.8.2021 Lexow : PHP 8-Fix - Parameter getauscht
		if ($query_id != -1) {
			$this->query_id = $query_id;
		}
		// 22.8.2021 Lexow : PHP 8-Fix - Seek eingebaut da seit PHP 8 der fetch anders arbeitet
		mysqli_field_seek($this->query_id, $offset);
		$finfo = @mysqli_fetch_field($this->query_id);
		return $finfo->name;
	}
	//   function get_fieldtype($query_id = -1, $offset) {
  function get_fieldtype($offset, $query_id = -1) {  // 22.8.2021 Lexow : PHP 8-Fix - Parameter getauscht 
    if ($query_id != -1) {
      $this->query_id = $query_id;
    }
		// 22.8.2021 Lexow : PHP 8-Fix - Seek eingebaut da seit PHP 8 der fetch anders arbeitet
		mysqli_field_seek($this->query_id, $offset);
		$finfo = @mysqli_fetch_field($this->query_id);
		return $finfo->type;
  }

  function affected_rows() {
    return ($this->connection) ? @mysqli_affected_rows($this->connection) : 0;
  }

  function is_empty($query = "") {
    if ($query != "") {
      $this->query($query);
    }
    return (!mysqli_num_rows($this->query_id)) ? 1 : 0;
  }

  function not_empty($query = "") {
    if ($query != "") {
      $this->query($query);
    }
    return (!mysqli_num_rows($this->query_id)) ? 0 : 1;
  }

  function get_table_fields($table) {
    if (!empty($this->table_fields[$table])) {
      return $this->table_fields[$table];
    }
    $this->table_fields[$table] = array();
    $result = $this->query("SHOW FIELDS FROM $table");
    while ($row = $this->fetch_array($result)) {
      $this->table_fields[$table][$row['Field']] = $row['Type'];
    }
    return $this->table_fields[$table];
  }

  function error($errmsg, $halt = 0) {
    if (!$this->no_error) {
      global $user_info;
      if (!defined("4IMAGES_ACTIVE") || (isset($user_info['user_level']) && $user_info['user_level'] == ADMIN)){
        echo "<br /><font color='#FF0000'><b>DB Error</b></font>: ".$errmsg."<br />";
      } else {
        echo "<br /><font color='#FF0000'><b>An unexpected error occured. Please try again later.</b></font><br />";
      }
      if ($halt) {
        exit;
      }
    }
  }
} // end of class
?>
